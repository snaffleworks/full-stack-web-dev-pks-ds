<?php
trait Hewan1 {
  public function nama1() {
    echo "Elang, ";
  }
}

trait Hewan2 {
  public function nama2() {
    echo "Harimau,";
  }
}

trait Hewan3 {
    public function darah() {
      echo "HP : 50, ";
    }
  }
  
  trait Hewan4 {
    public function kaki1() {
      echo "Berkaki 2,";
    }
  }

  trait Hewan5 {
    public function kaki2() {
      echo "Berkaki 4,";
    }
  }

  trait Hewan6 {
    public function skill1() {
      echo " Terbang tinggi,";
    }
  }

  trait Hewan7 {
    public function skill2() {
      echo " Lari cepat,";
    }
  }

  trait Fight1 {
    public function attpwr1() {
      echo " AttackPower = 10, ";
    }
  }
  
  trait Fight2 {
    public function attpwr2() {
      echo " AttackPower = 7,";
    }
  }
  
  trait Fight3 {
      public function defpwr1() {
        echo " Defense Power = 5, ";
      }
    }
    
    trait Fight4 {
      public function defpwr2() {
        echo " Defense Power = 8, ";
      }
    }

class ELANG {
  use Hewan1, Hewan3, Hewan4, Hewan6, Fight1, Fight3;
}

class HARIMAU {
  use Hewan2, Hewan3, Hewan5, Hewan7, Fight2, Fight4;
}

$obj = new ELANG();
$obj->nama1();
$obj->darah();
$obj->kaki1();
$obj->skill1();
$obj->attpwr1();
$obj->defpwr1();
echo "<br>";

$obj2 = new HARIMAU();
$obj2->nama2();
$obj2->darah();
$obj2->kaki2();
$obj2->skill2();
$obj2->attpwr2();
$obj2->defpwr2();
?>