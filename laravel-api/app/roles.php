<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = [ 'id' , 'name'  ];

    protected $keyType = 'string';

    public $incrementing = false;

    public static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if(empty($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }

}
